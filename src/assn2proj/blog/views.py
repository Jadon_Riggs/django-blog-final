from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Blog, Comment

from time import strftime
from datetime import datetime
from django.utils import timezone

def index(request):
    return render(request, 'blog/index.html', { 'now': strftime('%c')})

def bio(request):
    return render(request, 'blog/bio.html', { 'now': strftime('%c')})

def techtips(request):
    return render(request, 'blog/techtips.html', { 'now': strftime('%c')})

def archive(request):
    all_blogs_list = Blog.objects.all().order_by('-pub_date')
    context = {'all_blogs_list' : all_blogs_list}
    return render(request, 'blog/archive.html', context)

def home(request):
    recent_blogs_list = Blog.objects.order_by('-pub_date')[:3]
    context = {'recent_blogs_list' : recent_blogs_list}
    return render(request, 'blog/home.html', context)

def detail(request, blog_id):
    
    blog_post = get_object_or_404(Blog, pk=blog_id)
    return render(request, 'blog/detail.html', { 'blog_post' : blog_post })

def nuke(request):
    for q in Blog.objects.all():
        q.delete()
    return HttpResponseRedirect(reverse('blog:archive'))

def post(request, blog_id):
    now = timezone.now()
    blog_post = get_object_or_404(Blog, pk=blog_id)
    
    blog_post.comment_set.create(author_com = request.POST['author_com'],pub_date=now, content_com = request.POST['content_com'], email = request.POST['email'])
    blog_post.save()
    
    return HttpResponseRedirect(reverse('blog:detail', args=(blog_post.id,)))

def init(request):
    nuke(request)
    now = timezone.now()
    for i in range(5):
        
        q = Blog(title_blog="The Best Recipe for Hacking", author_blog="Jadon Riggs", pub_date = now.strftime('%Y-%m-%d %H:%M'), content_blog="Nam ornare risus quis massa cursus, et tristique ex sollicitudin. Aliquam id faucibus metus. Nunc porta auctor neque at lobortis. Cras porttitor felis ac sapien fringilla sodales. Maecenas tempor pharetra risus, nec fermentum ligula accumsan sit amet. Vestibulum dui lorem, varius ac lectus sed, pharetra bibendum tellus. Proin dapibus ut arcu in pharetra. Duis massa purus, volutpat sed dictum in, accumsan sed ex. Donec ligula justo, volutpat non erat sed, rhoncus commodo lacus. Maecenas varius rhoncus mi ac congue. Nullam vestibulum libero non leo pulvinar, sed tincidunt nibh rutrum. Nam blandit nisl vitae purus suscipit volutpat venenatis quis orci. Duis risus erat, molestie vitae urna ut, mattis pretium massa.dapibus ut arcu in pharetra. Duis massa purus, volutpat sed dictum in, accumsan sed ex. Donec ligula justo, volutpat non erat sed, rhoncus commodo lacus. Maecenas varius rhoncus mi ac congue. Nullam vestibulum libero non leo pulvinar, sed tincidunt nibc Maecenas varius rhoncus mi ac congue. Nullam vestibulum libero non leo pulvinar, sed tincidunt nibh rutrum. Nam blandit nisl vitae purus suscipit volutpat venenatis quis orci. Duis risus erat, molestie vitae urna ut, mattis pretium massa.dapibus ut arcu in pharetra. Duis massa purus, volutpat sed dictum in, accumsan sed ex. Donec ligula justo, volutpat non erat sed, rhoncus commodo lacus. Maecenas varius rhoncus mi ac congue. Nullam vestibulum libero non leo pulvinar, sed tincidunt nibcDonec ligula justo, volutpat non erat sed, rhoncus commodo lacus. Maecenas varius rhoncus mi ac congue. Nullam vestibulum libero non leo pulvinar, sed tincidunt nibc Maecenas varius rhoncus mi ac congue. Nullam vestibulum libero non leo pulvinar, sed tincidunt nibh rutrum. Nam blandit nisl vitae purus suscipit volutpat venenatis quis orci. Duis risus erat, molestie vitae urna ut, mattis pretium massa.dapibus ut arcu in pharetra. Duis massa purus, volutpat sed dictum in, accumsan sed ex. Donec ligula justo, volutpat non erat sed, rhoncus commodo lacus. Maecenas varius rhoncus mi ac congue.")
        
        q.save()
       
        for j in range(4):
            q.comment_set.create(author_com = "Hacker-man", pub_date = now.strftime('%Y-%m-%d %H:%M'), content_com = "this is fake news. Im gonna hack you bro. get wrecked. Nam blandit nisl vitae purus suscipit volutpat venenatis quis orci. Duis risus erat, molestie vitae urna ut, mattis pretium massa.dapibus ut arcu in pharetra. Duis massa purus, volutpat sed dictum in, accumsan sed ex.", email = "hackerman@gmail.com")
            q.save()
    
   

    return HttpResponseRedirect(reverse('blog:home'))
