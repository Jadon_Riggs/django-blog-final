
from django.urls import path

from . import views

app_name = 'blog'


urlpatterns = [
    path('', views.index, name='index'),
    path('bio', views.bio, name='bio'),
    path('techtips', views.techtips, name='techtips'),
    path('archive', views.archive, name='archive'),
    path('nuke', views.nuke, name='nuke'),
    path('init', views.init, name='init'),
    path('home', views.home, name='home'),
    path('<int:blog_id>/', views.detail, name='detail'),
    path('<int:blog_id>/post', views.post, name = 'post')

               ]
