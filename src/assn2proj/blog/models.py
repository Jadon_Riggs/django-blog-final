
from django.db import models


class Blog(models.Model):
    title_blog = models.CharField(max_length=30)
    author_blog = models.CharField(max_length=30)
    pub_date = models.DateTimeField('Date Published')
    content_blog = models.TextField()

    def __str__(self):
        return self.title_blog

class Comment(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    author_com = models.CharField(max_length=30)
    pub_date = models.DateTimeField('Date Published')
    content_com = models.TextField()
    email = models.CharField(max_length=100, default="email.com")
    

    def __str__(self):
        return self.author_com
